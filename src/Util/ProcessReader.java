package Util;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

public class ProcessReader {

    public static String getProcessOutput(Process p) {
        StringBuilder sb = new StringBuilder();

        Scanner s = new Scanner(p.getInputStream());
        while (s.hasNextLine()) {
            sb.append(s.nextLine()).append("\n");
        }
        return sb.toString();
    }

    public static void putProcessOutput(Process p, JTextPane tp) {
        try {
            Document document = tp.getDocument();
             SimpleAttributeSet attributes = new SimpleAttributeSet();
                StyleConstants.setForeground(attributes, Color.YELLOW); 
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

            while ((p.isAlive()) || (stdInput.ready())) {
                if (stdInput.ready()) {
                    try {
                        while (stdInput.ready()) {
                            int offset = document.getLength();
                            try {
                                document.insertString(offset, stdInput.readLine() + "\n", attributes);
                            } catch (BadLocationException ex) {
                                ex.printStackTrace();
                            }
                    tp.setCaretPosition(document.getLength());
                        }
                    } catch (IOException ex) {
                        if (!ex.getMessage().contains("Stream closed")) {
                            ex.printStackTrace();
                        }
                    }
                }
                Thread.sleep(300);
            }
        } catch (IOException ex) {
            if (!ex.getMessage().contains("Stream closed")) {
                ex.printStackTrace();
            }
        } catch (NoSuchMethodError n) {
            String message = "Please uninstall all versions of Java and get the latest version.";
            MessageBox.show(message, "Error", 2);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
