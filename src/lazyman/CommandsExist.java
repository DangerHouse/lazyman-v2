package lazyman;

import Util.MessageBox;
import Util.Props;
import Util.ProcessReader;
import java.io.IOException;

public class CommandsExist {

    private static String pip = "pip", python = "python";

    public static void set() {
        String pythonVer;
        if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            pip = "/usr/local/bin/pip";
            python = "/usr/local/bin/python";
        }
        try {
            pythonVer = ProcessReader.getProcessOutput(new ProcessBuilder("/bin/sh", "-c", python + " -V").start());

            if (pythonVer.contains("2.") || pip.contains("3.")) {
                try {
                    ProcessReader.getProcessOutput(new ProcessBuilder("/bin/sh", "-c", python + "3 -V").start());
                    pip = pip + "2";
                    python = python + "2";
                } catch (IOException ex) {
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            MessageBox.show("Please install Python.", "Python not found", 2);
        }
    }

    public static boolean pipCmdExists() {
        set();
        try {
            if (!cmdExists(pip)) {
                String p = "echo \'" + Props.getPW() + "\' | sudo -S ";
                System.err.println("Pip not found. Installing...");
                Process g = new ProcessBuilder("/bin/sh", "-c", "curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py").redirectErrorStream(true).start();
                System.err.println(ProcessReader.getProcessOutput(g));
                g = new ProcessBuilder("/bin/sh", "-c", p + python + " get-pip.py").redirectErrorStream(true).start();
                System.err.println(ProcessReader.getProcessOutput(g));
                g = new ProcessBuilder("/bin/sh", "-c", p + "rm get-pip.py").redirectErrorStream(true).start();
                System.err.println(ProcessReader.getProcessOutput(g));
            }
            return cmdExists(pip);
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static boolean livestreamerCmdExists() {
        set();
        if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            return cmdExists("/usr/local/bin/livestreamer");
        } else {
            return cmdExists("livestreamer");
        }
    }

    public static boolean installLivestreamer() {
        set();
        pipCmdExists();
        try {
            String p = "echo \'" + Props.getPW() + "\' | sudo -S ";
            System.err.println("Livestreamer not found... Installing...");
            Process l = new ProcessBuilder("/bin/sh", "-c", p + pip + " install livestreamer").redirectErrorStream(true).start();
            System.err.println(ProcessReader.getProcessOutput(l));

            if (System.getProperty("os.name").toLowerCase().contains("mac")) {
                return cmdExists("/usr/local/bin/livestreamer");
            } else {
                return cmdExists("livestreamer");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public static boolean pycryptoExists() {
        set();
        return cmdExists(pip + " show pycrypto");
    }

    public static boolean installPycrypto() {
        set();
        pipCmdExists();

        try {
            System.err.println("Pycrypto not found. Installing...");
            System.out.println(ProcessReader.getProcessOutput(new ProcessBuilder("/bin/sh", "-c", pip + " install --user pycrypto").redirectErrorStream(true).start()));
            return cmdExists(pip + " show pycrypto");
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private static boolean cmdExists(String cmd) {
        String output;
        try {
            output = ProcessReader.getProcessOutput(new ProcessBuilder("/bin/sh", "-c", cmd).redirectErrorStream(true).start()).toLowerCase();
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        
        return !(output.equals("")
                || output.contains("not found")
                || output.contains("no file")
                || output.contains("no such"));
    }
}
